import json
import sys
import os
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities    
from selenium.webdriver.firefox.options import Options
import traceback
import requests

def get_status():
    # We need to determine the Status cod from the geckodriver.log and return it
    debugLog = os.popen("awk '/webdriver::server/ && /DEBUG/ && /<-/' /projects/se-new/se-gecko/geckodriver.log").read()
    strDebugLog = str(debugLog)
    # print(strDebugLog)
    logs = strDebugLog.split("<-")
    logs.pop(0) # remove the first element since it is not a status code
    # print(len(logs))
    # print(logs)
    # print("\n\n")
    status_codes = []
    for log in logs:
        strLog = log.strip()
        status_codes.append(strLog[0:3])
    status_codes.sort(reverse=True)
    # print(status_codes)
    return status_codes[0]

def loadUrlPage(url):

    url=("http://"+str(url) if url[0:4] !="http" else url)

    options = webdriver.FirefoxOptions()
    options.log.level = "trace"
    options.add_argument('-headless') 
    options.add_argument("--enable-javascript")
    options.add_argument("user-agent=Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36")
    # enable browser logging
    d = DesiredCapabilities.FIREFOX
    d['loggingPrefs'] = { 'performance':'ALL' }
    driver = webdriver.Firefox(executable_path="/projects/se-new/se-gecko/geckodriver", capabilities=d, options=options)

    page_source = header = data = errorMsg = ''
    error = False
    
    # Load the Page
    try:
        driver.get(url)
        time.sleep(5)
        page_source=driver.page_source  

    except Exception as e:
        errorMsg += "Selenium was not able to get the page"
        errorMsg += "\n" + str(e)
        errorMsg += "\n" + str(traceback.format_exc())
        error = True

    except TimeoutException:
        errorMsg = "Page load time out"
        error = True

    # Get the header
    try:
        header = driver.execute_script("var req = new XMLHttpRequest();req.open('GET', document.location, false);req.send(null);return req.getAllResponseHeaders()")

    except Exception as e:
        errorMsg += "Error fetching header"
        errorMsg += "\n" + str(e)
        errorMsg += "\n" + str(traceback.format_exc())
        error = True

    driver.close()
    status_code = get_status()
    data = json.dumps({"error":error,"errorMsg":errorMsg,"page_source":page_source,"header":header,"status_code":status_code})
    print(data)
    return True

# delete old geckodriver.log
try:
    os.remove('/projects/se-new/se-gecko/geckodriver.log')
except:
    pass

# url = 'qap.questcdn.com/qap/projects/prj_browse/ipp_prj_browse.html?group=2319272&provider=2319272'
# url = 'http://www.demandstar.com/supplier/bids/agency_inc/bid_list.asp?f=search&mi=10312'

# Passing the URL as an argument yields unexpected results when a URL is redirected
# url=sys.argv[1]

# read url from file
with open('/projects/se-new/se-gecko/test.txt') as f:
    url = f.read()

loadUrlPage(url)

